//tslint:disable
const bodyParser = require("body-parser");
const serverless = require("serverless-http");
const express = require("express");
const cors = require("cors");
const app = express();
const expressValidator = require("express-validator");
import notificationsRouter from "./src/routers/notificationsRouter";

app.use(
  cors({
    origin: "http://localhost:3000",
    credentials: true,
  }),
);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());

app.use("/", notificationsRouter);

export const handler = serverless(app);
