//tslint:disable
import { config as getConfig } from "../extra/config";
import axios from "axios";

const envConfig = getConfig();

const base_url = "https://euw1.api.riotgames.com";

const setBaseUrl = (sn: string) => {
  switch (sn) {
    case "euw":
      return "https://euw1.api.riotgames.com";
    case "ru":
      return "https://ru.api.riotgames.com";
    case "eune":
      return "https://eun1.api.riotgames.com";
    case "br":
      return "https://br1.api.riotgames.com";
    case "na":
      return "https://na1.api.riotgames.com";
    default:
      return "https://ru.api.riotgames.com";
  }
};

const _axios = axios.create({
  baseURL: "",
  timeout: 1000,
  headers: {
    "X-Riot-Token": "RGAPI-5f2a7ffa-465f-43e5-891b-bc7b0dab629f",
  },
});

class NotificationController {
  check = async (req, res, next) => {
    try {
      res.status(200).send({ res: "YO" });
    } catch (e) {
      next(e);
    }
  };

  getSummonerId = async (req, res, next) => {
    const { summonerName, server } = req.query;

    try {
      const data = await _axios.get(
        encodeURI(
          `${setBaseUrl(
            server,
          )}/lol/summoner/v4/summoners/by-name/${summonerName}`,
        ),
      );

      console.log(data.data.id);

      res.status(200).send({ id: data.data.id });
    } catch (err) {
      res.send(err);
    }
  };

  getSummonerInfo = async (req, res, next) => {
    const { summonerId, server } = req.query;

    try {
      const data = await _axios.get(
        encodeURI(
          `${setBaseUrl(
            server,
          )}/lol/league/v4/entries/by-summoner/${summonerId}`,
        ),
      );

      res.status(200).send({ data: data.data });
    } catch (e) {
      res.send(e);
    }
  };
}

export default new NotificationController();
