import bodyParser from "body-parser";
import { Router } from "express";
// tslint:disable-next-line:no-relative-imports
import lolController from "../controllers/lolController";

const router = Router();

router.use(bodyParser.json());
//@ts-ignore
router.get("/getInfo", lolController.check);
router.get("/summonerId", lolController.getSummonerId);
router.get("/summonerInfo", lolController.getSummonerInfo);

export default router;

//summoner-name => summonerID
///lol/league/v4/entries/by-summoner/{encryptedSummonerId}
