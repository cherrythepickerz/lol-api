import dotenv from "dotenv";
dotenv.config();

export const config = () => {
  // return {
  //   region: process.env.AWS_DEFAULT_REGION,
  //   apiVersion: process.env.AWS_SNS_API_VESRION,
  //   awsPlatformArnIos: process.env.AWS_PLATFORM_ARN_IOS,
  //   awsPlatformArnAndroid: process.env.AWS_PLATFORM_ARN_ANDROID,
  // };

  return {
    region: process.env.awsDefaultRegion,
    apiVersion: process.env.awsSnsApiVersion,
    awsPlatformArnIos: process.env.awsPlatformArnIos,
    awsPlatformArnAndroid: process.env.awsPlatformArnAndroid,
  };
};
