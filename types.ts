export type message = {
  title: string;
  body: string;
  target: string;
};